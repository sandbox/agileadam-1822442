<?php

/**
 * @file Adds supplier stock support to feeds
 *
 * Provides a new "Supplier SKU (unique)" Feeds mapping target that will match
 * products based on their supplier SKU. This way, you can update existing products based
 * on their Product SKU (sku) OR their Supplier SKU (field_supplier_sku).
 *
 * Provides a new "UPC (unique)" Feeds mapping target that will match
 * products based on their UPC code. This way, you can update existing products based
 * on their Product SKU (sku) OR their Supplier SKU (field_supplier_sku) OR UPC (field_upc).
 *
 * This functionality is dependent on http://drupal.org/node/661606#comment-6481214
 */

/**
 * Implements hook_feeds_processor_targets_alter().
 *
 * @see field_feeds_processor_targets_alter().
 */
function commerce_supplier_stock_feeds_processor_targets_alter(&$targets, $entity_type, $bundle_name) {
  $targets['field_supplier_sku_unique']['name'] = 'Supplier SKU (unique)';
  $targets['field_supplier_sku_unique']['optional_unique'] = TRUE;
  $targets['field_supplier_sku_unique']['description'] = t('This is used to lookup the existing product.');
  $targets['field_supplier_sku_unique']['bundle_name'] = $bundle_name;
  // See http://drupal.org/node/661606#comment-6481214
  $targets['field_supplier_sku_unique']['unique_callbacks'] = array('commerce_supplier_stock_feeds_existing_entity_id_by_supplier_sku');

  $targets['field_upc_unique']['name'] = 'UPC (unique)';
  $targets['field_upc_unique']['optional_unique'] = TRUE;
  $targets['field_upc_unique']['description'] = t('This is used to lookup the existing product.');
  $targets['field_upc_unique']['bundle_name'] = $bundle_name;
  // See http://drupal.org/node/661606#comment-6481214
  $targets['field_upc_unique']['unique_callbacks'] = array('commerce_supplier_stock_feeds_existing_entity_id_by_upc');
}

/**
 * Callback for Feeds field_supplier_sku_unique unique target
 *
 * @param string $target
 *   The unique field name, containing the value to be checked.
 * @param int|string $value
 *   The unique field value to be checked.
 * @param string $entity_type
 *   Entity type for the entity to be processed.
 * @param string $bundle_name
 *   Bundle name for the entity to be processed.
 *
 * @return int
 *   Feeds processor existing entity ID.
 *
 * @see commerce_supplier_stock_feeds_processor_targets_alter()
 * @see FeedsProcessor::existingEntityId()
 */
function commerce_supplier_stock_feeds_existing_entity_id_by_supplier_sku($target, $value, $entity_type, $bundle_name) {
  // Instead of querying the DB every time we need a product ID based on Supplier SKU, lets use this function to
  // cache the results and just lookup the value within those results.
  $products_with_supplier_sku = commerce_supplier_stock_feeds_all_products_with_a_supplier_sku($entity_type, $bundle_name);

  // Returns the product_id if there's a match, otherwise it will return 0
  if (isset($products_with_supplier_sku['products'][$value])) {
    return $products_with_supplier_sku['products'][$value];
  }
  else {
    return 0;
  }
}

/**
 * Looks up products and their Supplier SKU and static-caches the results for speed.
 * Other functions can use this to quickly lookup a product_id based on a Supplier SKU
 * This will always use the cached version of the data. Use drupal_static_reset()
 * if you need to force re-query the DB.
 *
 * This will only find products that have Supplier SKU (to make it more efficient)
 * Also for efficiency we use Supplier SKU as the key so we can do an isset() to find the
 * product_key instead of an array_search to search for a Supplier SKU and return its value.
 * isset() is far quicker. See http://stackoverflow.com/a/9183314
 *
 * @param string $entity_type
 *   Entity type for the entity to be processed.
 * @param string $bundle_name
 *   Bundle name for the entity to be processed.
 *
 * @return array
 *   Static-cached array of items. Most importantly, the cached array contains a subarray
 *   of products (key = Supplier SKU, val = product_id)
 *
 * @see commerce_supplier_stock_feeds_existing_entity_id_by_supplier_sku()
 */
function commerce_supplier_stock_feeds_all_products_with_a_supplier_sku($entity_type, $bundle_name) {
  $cached_data = &drupal_static(__FUNCTION__);
  if (!isset($cached_data) || $cached_data['entity_type'] != $entity_type || $cached_data['bundle_name'] != $bundle_name) {
    $cached_data['entity_type'] = $entity_type;
    $cached_data['bundle_name'] = $bundle_name;

    $query = db_select('field_data_field_supplier_sku', 's');
    $query->fields('s', array('entity_id', 'field_supplier_sku_value'))
          ->condition('s.bundle', $bundle_name)
          ->condition('s.entity_type', $entity_type)
          ->condition('s.deleted', 0)
          ->condition('s.field_supplier_sku_value', '', '<>')
          ->addMetaData('account', user_load(1));

    $result = $query->execute();
    foreach ($result as $product) {
      if (!empty($product->field_supplier_sku_value)) {
        $cached_data['products'][$product->field_supplier_sku_value] = $product->entity_id;
      }
    }
  }

  return $cached_data;
}

/**
 * Callback for Feeds field_upc_unique unique target
 *
 * @param string $target
 *   The unique field name, containing the value to be checked.
 * @param int|string $value
 *   The unique field value to be checked.
 * @param string $entity_type
 *   Entity type for the entity to be processed.
 * @param string $bundle_name
 *   Bundle name for the entity to be processed.
 *
 * @return int
 *   Feeds processor existing entity ID.
 *
 * @see commerce_supplier_stock_feeds_processor_targets_alter()
 * @see FeedsProcessor::existingEntityId()
 */
function commerce_supplier_stock_feeds_existing_entity_id_by_upc($target, $value, $entity_type, $bundle_name) {
  // Instead of querying the DB every time we need a product ID based on UPC, lets use this function to
  // cache the results and just lookup the value within those results.
  $products_with_upcs = commerce_supplier_stock_feeds_all_products_with_a_upc($entity_type, $bundle_name);

  // Returns the product_id if there's a match, otherwise it will return 0
  if (isset($products_with_upcs['products'][$value])) {
    return $products_with_upcs['products'][$value];
  }
  else {
    return 0;
  }
}

/**
 * Looks up products and their UPC codes and static-caches the results for speed.
 * Other functions can use this to quickly lookup a product_id based on a UPC code
 * This will always use the cached version of the data. Use drupal_static_reset()
 * if you need to force re-query the DB.
 *
 * This will only find products that have UPC codes (to make it more efficient)
 * Also for efficiency we use UPC as the key so we can do an isset() to find the
 * product_key instead of an array_search to search for a UPC and return its value.
 * isset() is far quicker. See http://stackoverflow.com/a/9183314
 *
 * @param string $entity_type
 *   Entity type for the entity to be processed.
 * @param string $bundle_name
 *   Bundle name for the entity to be processed.
 *
 * @return array
 *   Static-cached array of items. Most importantly, the cached array contains a subarray
 *   of products (key = UPC, val = product_id)
 *
 * @see commerce_supplier_stock_feeds_existing_entity_id_by_upc()
 */
function commerce_supplier_stock_feeds_all_products_with_a_upc($entity_type, $bundle_name) {
  $cached_data = &drupal_static(__FUNCTION__);
  if (!isset($cached_data) || $cached_data['entity_type'] != $entity_type || $cached_data['bundle_name'] != $bundle_name) {
    $cached_data['entity_type'] = $entity_type;
    $cached_data['bundle_name'] = $bundle_name;

    $query = db_select('field_data_field_upc', 'u');
    $query->fields('u', array('entity_id', 'field_upc_value'))
          ->condition('u.bundle', $bundle_name)
          ->condition('u.entity_type', $entity_type)
          ->condition('u.deleted', 0)
          ->condition('u.field_upc_value', '', '<>')
          ->addMetaData('account', user_load(1));

    $result = $query->execute();
    foreach ($result as $product) {
      if (!empty($product->field_upc_value)) {
        $cached_data['products'][$product->field_upc_value] = $product->entity_id;
      }
    }
  }

  return $cached_data;
}
