GENERAL
-------------
This module adds some rules actions to deduct not only simple stock, but also
what I'm calling Supplier Stock. Sometimes you may want to be able to sell
a product even though you don't have the item in your posession. So, in this case
we'd track two stock values (local stock and supplier stock). So, when local stock runs
out we would switch over and sell supplier stock, which are items we don't currently
have but could dropship to the customer.

REQUIREMENTS
-------------
This module requires that products are purchasable even if their stock level is less than 1.
To achieve this, make sure you use Commerce Stock v2. Clone the three rules:
	rules_stock_disable_add_to_cart
	rules_stock_validate_add_to_cart
	rules_stock_validate_checkout
Then, edit them all and add two conditions, as illustrated at the
bottom of this README (Rules 1-3). You add the exact same two conditions to all three.

INSTALLATION
-------------
Sorry this stuff isn't automated.

1) Enable this module
2) Clone the rules_stock_decrease_when_completing_the_order_process rule
   Remove the action "Decrease the product stock level, given a line item"
   Replace with "Decrease the product stock (local and/or supplier) level, given a line item"
   (See Rule 4 definition at bottom of this README)
3) Disable original rules_stock_decrease_when_completing_the_order_process rule
4) Update all line item types to include a field_stock_source field
   Text select list: local|Local  supplier|Supplier
5) Create new rule: rules_supplier_stock_set_source_before_saving_commerce_line_item
   (See Rule 5 definition at bottom of this README)

CONFIGURATION
-------------

TESTING / USAGE
---------------

RULES
---------------
Rules 1-3: note the last two conditions!
{ "rules_stock_disable_add_to_cart_custom" : {
    "LABEL" : "Stock: disable add to cart (custom)",
    "PLUGIN" : "reaction rule",
    "REQUIRES" : [ "commerce_ss", "rules", "commerce_stock" ],
    "ON" : [ "commerce_stock_check_add_to_cart_form_state" ],
    "IF" : [
      { "commerce_ss_stock_enabled_on_product" : { "commerce_product" : [ "commerce_product" ] } },
      { "commerce_ss_stock_not_disabled" : { "commerce_product" : [ "commerce_product" ] } },
      { "NOT data_is" : {
          "data" : [ "commerce-product:commerce-stock" ],
          "op" : "\u003E",
          "value" : "0"
        }
      },
      { "entity_has_field" : { "entity" : [ "commerce-product" ], "field" : "field_supplier_stock" } },
      { "data_is" : {
          "data" : [ "commerce-product:field-supplier-stock" ],
          "op" : "\u003C",
          "value" : "2"
        }
      }
    ],
    "DO" : [
      { "commerce_stock_set_add_to_cart_form_state" : { "disabled" : 1, "text" : "Out of stock", "class_name" : "out-of-stock" } }
    ]
  }
}

Rule 4:
{ "rules_stock_decrease_when_completing_the_order_process_custom" : {
    "LABEL" : "Stock: Decrease when completing the order process (custom)",
    "PLUGIN" : "reaction rule",
    "REQUIRES" : [ "commerce_supplier_stock", "commerce_checkout" ],
    "ON" : [ "commerce_checkout_complete" ],
    "DO" : [
      { "LOOP" : {
          "USING" : { "list" : [ "commerce-order:commerce-line-items" ] },
          "ITEM" : { "list_item" : "Current list item" },
          "DO" : [
            { "commerce_supplier_stock_decrease_by_line_item" : { "commerce_line_item" : [ "list-item" ] } }
          ]
        }
      }
    ]
  }
}

Rule 5:
{ "rules_supplier_stock_set_source_before_saving_commerce_line_item" : {
    "LABEL" : "Supplier stock: set source before saving commerce line item",
    "PLUGIN" : "reaction rule",
    "REQUIRES" : [ "rules", "entity" ],
    "ON" : [ "commerce_line_item_presave" ],
    "IF" : [
      { "entity_has_field" : { "entity" : [ "commerce-line-item" ], "field" : "commerce_product" } },
      { "entity_has_field" : { "entity" : [ "commerce-line-item" ], "field" : "field_stock_source" } },
      { "data_is" : {
          "data" : [ "commerce-line-item:commerce-product:commerce-stock" ],
          "op" : "\u003C",
          "value" : "1"
        }
      }
    ],
    "DO" : [
      { "data_set" : {
          "data" : [ "commerce-line-item:field-stock-source" ],
          "value" : "supplier"
        }
      },
      { "drupal_message" : {
          "message" : "This product is available from the supplier. Please be advised it will require an additional time frame on supplier \/ special order items. We will confirm availability within 24 hours in most cases.",
          "type" : "warning",
          "repeat" : 0
        }
      }
    ]
  }
}
