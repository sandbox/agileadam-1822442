<?php

/**
 * @file
 * Rules integration for Commerce Supplier Stock
 *
 * This is largely a duplication of commerce_stock_ss.rules.inc
 */

/**
 *
 * Implementation of hook_rules_action_info().
 */
function commerce_supplier_stock_rules_action_info() {
  $actions = array();

  $actions['commerce_supplier_stock_decrease_by_line_item'] = array(
    'label' => t('Decrease the product stock (local and/or supplier) level, given a line item'),
    'group' => t('Commerce Supplier Stock'),
    'parameter' => array(
      'commerce_line_item' => array(
        'type' => 'commerce_line_item',
        'label' => t('Line item')
      ),
    ),
  );

  $actions['commerce_supplier_stock_increase_by_line_item'] = array(
    'label' => t('Increase the product stock (local and/or supplier) level, given a line item'),
    'group' => t('Commerce Supplier Stock'),
    'parameter' => array(
      'commerce_line_item' => array(
        'type' => 'commerce_line_item',
        'label' => t('Line item')
      ),
    ),
  );


  return $actions;
}


/**
 * If the line item is stock-enabled, subtract the sold amount in a line item
 * from stock.
 *
 * @param $line_item
 *   A line item object.
 */
function commerce_supplier_stock_decrease_by_line_item($line_item) {
  if(in_array($line_item->type, commerce_product_line_item_types())) {
    // The product SKU that will have its stock level adjusted.
    $sku = $line_item->line_item_label;
    $product = commerce_product_load_by_sku($sku);
    if (commerce_ss_product_type_enabled($product->type)) {
      if (!(commerce_ss_product_type_override_enabled($product->type)
          && isset($product->commerce_stock_override['und']) && $product->commerce_stock_override['und'][0]['value'] == 1)) {

        $qty = $line_item->quantity;
        $stock_source = $line_item->field_stock_source[LANGUAGE_NONE][0]['value'];
        // Subtract the sold amount from the available stock level.
        commerce_supplier_stock_stock_adjust($product, -$qty, $stock_source);
      }
    }
  }
}


/**
 * If the line item is stock-enabled, add the sold amount in a line item
 * to stock. Typically used when a line item is removed from an order
 * (as when items are added to and removed from cart).
 *
 * @param $line_item
 *   A line item object.
 */
function commerce_supplier_stock_increase_by_line_item($line_item) {
  if(in_array($line_item->type, commerce_product_line_item_types())) {
    // The product SKU that will have its stock level adjusted.
    $sku = $line_item->line_item_label;
    $product = commerce_product_load_by_sku($sku);
    if (commerce_ss_product_type_enabled($product->type)) {
      if (!(commerce_ss_product_type_override_enabled($product->type)
           && isset($product->commerce_stock_override['und']) && $product->commerce_stock_override['und'][0]['value'] == 1)) {

        $qty = $line_item->quantity;
        $stock_source = $line_item->field_stock_source[LANGUAGE_NONE][0]['value'];
        // Add the sold amount to the available stock level.
        commerce_supplier_stock_stock_adjust($product, $qty, $stock_source);
      }
    }
  }
}

/**
 * Adjusts a particular product SKU by a certain value.
 * A positive number will add to stock, a negative number will remove from
 * stock. Somewhat the equivalent of uc_stock_adjust().
 *
 * @param $product
 *   The product for which to change the stock level.
 * @param $qty
 *   The quantity to add to the stock level.
 */
function commerce_supplier_stock_stock_adjust($product, $qty, $stock_source) {
  if (!commerce_ss_product_type_enabled($product->type)) {
    return;
  }

  $wrapper = entity_metadata_wrapper('commerce_product', $product);

  if ($stock_source == 'local') {
    $new_stock = $wrapper->commerce_stock->value() + $qty;
    $wrapper->commerce_stock->set($new_stock);
  }
  else {
    $new_stock = $wrapper->field_supplier_stock->value() + $qty;
    $wrapper->field_supplier_stock->set($new_stock);
  }

  $result = $wrapper->save();

  // @todo should this be moved to the
  if ($result) {
    watchdog('commerce_stock', 'Modified %type stock level of product %sku by %amount', array('%type' => $stock_source, '%sku' => $product->sku, '%amount' => $qty));
  } else {
    watchdog('commerce_stock', 'Failed attempt to modify %type stock level of product %sku by %amount', array('%type' => $stock_source, '%sku' => $product->sku, '%amount' => $qty), WATCHDOG_ERROR);
  }
}
