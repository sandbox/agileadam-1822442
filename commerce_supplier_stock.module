<?php

/**
 * @file
 * Manages supplier stock levels for Drupal Commerce
 */

/**
 * Implements hook_theme();
 */
function commerce_supplier_stock_theme() {
  $items = array(
    'supplier_stock_status' => array(
      'variables' => array('count' => NULL),
    ),
  );

  return $items;
}

/**
 * Theme function to show supplier stock status (e.g., "In stock")
 *
 * @param array $variables
 *   $variables['count'] = the quantity
 * @return string
 *   HTML output to show overall stock status (e.g., "In stock")
 */
function theme_supplier_stock_status($variables) {
  // WARNING: If updating this logic, also update other occurrences of "prebuy-logic"
  if ($variables['count'] >= 9500 && $variables['count'] <= 9999) {
    return '<span class="stock-prebuy" title="Delivery in Fall of ' . date('Y') . '">' . t('Pre-buy now') . '</span>';
  }

  if ($variables['count'] <= 1) {
    $output = '<span class="stock-out">' . t('Sold out') . '</span>';
  }
  else if ($variables['count'] <= 10) {
    $output = '<span class="stock-low">' . t('Low stock') . '</span>';
  }
  else {
    $output = '<span class="stock-full">' . t('In stock') . '</span>';
  }

  return $output;
}
